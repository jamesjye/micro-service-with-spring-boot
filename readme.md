Toronto Java Users Group Presenation 

According to Martin Fowler “The term "Microservice Architecture" has sprung up over the last few years to describe a particular way of designing software applications as suites of independently deployable services. While there is no precise definition of this architectural style, there are certain common characteristics around organization around business capability, automated deployment, intelligence in the endpoints, and decentralized control of languages and data.” 

The presentation is going to explore the micro services architecture and how to implement micro services with spring boot.  The presentation consists of a few slides and lot of live coding with Spring Boot. No previous Spring experience required to follow along the live coding examples.


